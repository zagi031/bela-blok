# Bela blok

Bela blok is an Android app for taking notes while playing famous Croatian card game [Belot](https://en.wikipedia.org/wiki/Belote). While playing Belot, only one player needs to use this app for tracking result while playing. You can take notes of each playing round, edit or delete those notes. In case you make a mistake, there is a undo button that undos your previous action. Belot is played till 501, 701 or 1001 points, so this app lets you choose your playing mode. Also, if you exit the app, the results are stored and they are waiting for you to continue taking records of each round. 

To download .apk file, click [here](https://gitlab.com/zagi031/bela-blok/-/blob/master/bela-blok.apk).

Technologies used: Android, Kotlin, RecyclerView, Room
