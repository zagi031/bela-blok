package com.zagorscak.belablok.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.ViewModelProvider
import com.zagorscak.belablok.R
import com.zagorscak.belablok.interfaces.Common
import com.zagorscak.belablok.viewmodels.SplashScreenViewModel

class SplashScreen : AppCompatActivity() {
    private lateinit var viewModel: SplashScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.viewModel = ViewModelProvider(this).get(SplashScreenViewModel::class.java)
        setTheme()
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            val intent = Intent(this, ScoreActivity::class.java)
            this.startActivity(intent)
            this.finish()
        }, Common.SplashScreenDuration)
    }

    private fun setTheme() {
        if (viewModel.isEnabledDarkMode()) {
            setTheme(R.style.AppTheme_Dark)
        } else {
            setTheme(R.style.AppTheme_Light)
        }
    }
}
