package com.zagorscak.belablok.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.zagorscak.belablok.R
import com.zagorscak.belablok.interfaces.Common
import com.zagorscak.belablok.memento.GameState
import com.zagorscak.belablok.models.Note
import com.zagorscak.belablok.viewmodels.NewEntryViewModel
import kotlinx.android.synthetic.main.activity_new_entry.*

class NewEntryActivity : AppCompatActivity() {
    private lateinit var viewModel: NewEntryViewModel
    private lateinit var notes:List<Note>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NewEntryViewModel::class.java)
        setTheme()
        setContentView(R.layout.activity_new_entry)
        setUI()
        viewModel.getAllNotesFromRoom().observe(this, Observer {
            this.notes = it
        })
        viewModel.note.observe(this, Observer {
            rbtn_team1Points.text = it.getTeam1TotalPoints().toString()
            rbtn_team2Points.text = it.getTeam2TotalPoints().toString()
            setNumberPickerTeam1CallPoints(it.team1CallPoints)
            setNumberPickerTeam2CallPoints(it.team2CallPoints)
        })
        viewModel.toast.observe(this, Observer {
            showToast(it)
        })
    }

    private fun setTheme() {
        if (viewModel.isEnabledDarkMode()) {
            setTheme(R.style.AppTheme_Dark)
        } else {
            setTheme(R.style.AppTheme_Light)
        }
    }

    private fun setUI() {
        setDefaultMinAndMaxValuesOnNumberPickers()
        setListeners()

        if (intent.getSerializableExtra(Common.NewEntryObjectKEY) != null) {
            supportActionBar?.title = "Uredi upis"
            val note: Note = intent.getSerializableExtra(Common.NewEntryObjectKEY) as Note
            prepareNoteForEditing(note)
        } else supportActionBar?.title = "Novi upis"
    }

    private fun prepareNoteForEditing(note: Note) {
        setGamePointsNumberPicker(note.team1GamePoints)
        rbtn_team1Points.isChecked = true
        viewModel.setNoteID(note.id)
        viewModel.setTeam1GamePoints(note.team1GamePoints)
        viewModel.setTeam2GamePoints(note.team2GamePoints)
        viewModel.setTeam1CallPoints(note.team1CallPoints)
        viewModel.setTeam2CallPoints(note.team2CallPoints)
        if (np_Points1.value == 1) {
            np_Points2.maxValue = 6
        } else np_Points2.maxValue = 9

        if (np_Points1.value == 1 && np_Points2.value == 6) {
            np_Points3.maxValue = 2
        } else np_Points3.maxValue = 9

        if (np_Points1.value == 1 && np_Points2.value == 6) {
            np_Points3.maxValue = 2
        } else np_Points3.maxValue = 9
    }

    private fun setDefaultMinAndMaxValuesOnNumberPickers() {
        np_Points1.maxValue = 1
        np_Points1.minValue = 0
        np_Points2.maxValue = 9
        np_Points2.minValue = 0
        np_Points3.maxValue = 9
        np_Points3.minValue = 0

        np_CallPointsTeam1_1.maxValue = 9
        np_CallPointsTeam1_1.minValue = 0
        np_CallPointsTeam1_2.maxValue = 9
        np_CallPointsTeam1_2.minValue = 0

        np_CallPointsTeam2_1.maxValue = 9
        np_CallPointsTeam2_1.minValue = 0
        np_CallPointsTeam2_2.maxValue = 9
        np_CallPointsTeam2_2.minValue = 0
    }

    private fun setListeners() {
        np_Points1.setOnValueChangedListener { picker, oldVal, newVal ->
            if (newVal == 1) {
                np_Points2.maxValue = 6
            } else np_Points2.maxValue = 9

            if (newVal == 1 && np_Points2.value == 6) {
                np_Points3.maxValue = 2
            } else np_Points3.maxValue = 9

            if (rbtn_team1Points.isChecked) {
                viewModel.setTeam1GamePoints(getNumberFromGamePointsNumberPicker())
            } else if (rbtn_team2Points.isChecked) {
                viewModel.setTeam2GamePoints(getNumberFromGamePointsNumberPicker())
            }
        }

        np_Points2.setOnValueChangedListener { picker, oldVal, newVal ->
            if (np_Points1.value == 1 && newVal == 6) {
                np_Points3.maxValue = 2
            } else np_Points3.maxValue = 9

            if (rbtn_team1Points.isChecked) {
                viewModel.setTeam1GamePoints(getNumberFromGamePointsNumberPicker())
            } else if (rbtn_team2Points.isChecked) {
                viewModel.setTeam2GamePoints(getNumberFromGamePointsNumberPicker())
            }
        }

        np_Points3.setOnValueChangedListener { picker, oldVal, newVal ->
            if (rbtn_team1Points.isChecked) {
                viewModel.setTeam1GamePoints(getNumberFromGamePointsNumberPicker())
            } else if (rbtn_team2Points.isChecked) {
                viewModel.setTeam2GamePoints(getNumberFromGamePointsNumberPicker())
            }
        }

        np_CallPointsTeam1_1.setOnValueChangedListener { picker, oldVal, newVal ->
            viewModel.setTeam1CallPoints(getNumberFromTeam1CallPoints())
        }
        np_CallPointsTeam1_2.setOnValueChangedListener { picker, oldVal, newVal ->
            viewModel.setTeam1CallPoints(getNumberFromTeam1CallPoints())
        }

        np_CallPointsTeam2_1.setOnValueChangedListener { picker, oldVal, newVal ->
            viewModel.setTeam2CallPoints(getNumberFromTeam2CallPoints())
        }
        np_CallPointsTeam2_2.setOnValueChangedListener { picker, oldVal, newVal ->
            viewModel.setTeam2CallPoints(getNumberFromTeam2CallPoints())
        }

        tv_resetCallPointsTeam1.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_to_left))
            np_CallPointsTeam1_1.value = 0
            np_CallPointsTeam1_2.value = 0
            np_CallPointsTeam1_3.value = 0
            viewModel.setTeam1CallPoints(getNumberFromTeam1CallPoints())
        }
        tv_resetCallPointsTeam2.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_to_right))
            np_CallPointsTeam2_1.value = 0
            np_CallPointsTeam2_2.value = 0
            np_CallPointsTeam2_3.value = 0
            viewModel.setTeam2CallPoints(getNumberFromTeam2CallPoints())
        }

        rbtn_team1Points.setOnClickListener {
            viewModel.setTeam1GamePoints(getNumberFromGamePointsNumberPicker())
        }

        rbtn_team2Points.setOnClickListener {
            viewModel.setTeam2GamePoints(getNumberFromGamePointsNumberPicker())
        }

        btn_Entry.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(this, R.anim.jump))
            if (intent.getSerializableExtra(Common.NewEntryObjectKEY) != null) {
                if (viewModel.editNote()) {
                    viewModel.setState(
                        GameState(
                            viewModel.getTeam1Score(),
                            viewModel.getTeam2Score(),
                            notes
                        ).saveState().deepCopy()
                    )
                    btn_Entry.isEnabled = false
                    finish()
                } else {
                    val rbtn1 = findViewById<View>(R.id.rbtn_team1Points)
                    val rbtn2 = findViewById<View>(R.id.rbtn_team2Points)
                    rbtn1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale))
                    rbtn2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale))
                }
            } else {
                if (viewModel.addNote()) {
                    viewModel.setState(
                        GameState(
                            viewModel.getTeam1Score(),
                            viewModel.getTeam2Score(),
                            notes
                        ).saveState().deepCopy()
                    )
                    btn_Entry.isEnabled = false
                    finish()
                } else {
                    val rbtn1 = findViewById<View>(R.id.rbtn_team1Points)
                    val rbtn2 = findViewById<View>(R.id.rbtn_team2Points)
                    rbtn1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale))
                    rbtn2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale))
                }
            }
        }
    }

    private fun getNumberFromGamePointsNumberPicker(): Int {
        val result: String =
            np_Points1.value.toString() + np_Points2.value.toString() + np_Points3.value.toString()
        return result.toInt()
    }

    private fun getNumberFromTeam1CallPoints(): Int {
        val result =
            np_CallPointsTeam1_1.value.toString() + np_CallPointsTeam1_2.value.toString() + np_CallPointsTeam1_3.value.toString()
        return result.toInt()
    }

    private fun getNumberFromTeam2CallPoints(): Int {
        val result =
            np_CallPointsTeam2_1.value.toString() + np_CallPointsTeam2_2.value.toString() + np_CallPointsTeam2_3.value.toString()
        return result.toInt()
    }

    private fun setGamePointsNumberPicker(number: Int) {
        val num1 = number / 100
        val num2 = (number - num1 * 100) / 10
        val num3 = (number - num1 * 100) % 10
        np_Points1.value = num1
        np_Points2.value = num2
        np_Points3.value = num3
    }

    private fun setNumberPickerTeam1CallPoints(number: Int) {
        val num1 = number / 100
        val num2 = (number - num1 * 100) / 10
        val num3 = (number - num1 * 100) % 10
        np_CallPointsTeam1_1.value = num1
        np_CallPointsTeam1_2.value = num2
        np_CallPointsTeam1_3.value = num3
    }

    private fun setNumberPickerTeam2CallPoints(number: Int) {
        val num1 = number / 100
        val num2 = (number - num1 * 100) / 10
        val num3 = (number - num1 * 100) % 10
        np_CallPointsTeam2_1.value = num1
        np_CallPointsTeam2_2.value = num2
        np_CallPointsTeam2_3.value = num3
    }

    private fun showToast(message: String) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        val view = toast.view.findViewById<View>(android.R.id.message) as TextView
        view.gravity = Gravity.CENTER
        toast.show()
    }
}
