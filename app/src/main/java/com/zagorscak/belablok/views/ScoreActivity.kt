package com.zagorscak.belablok.views

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zagorscak.belablok.BelaBlok
import com.zagorscak.belablok.R
import com.zagorscak.belablok.adapters.NoteAdapter
import com.zagorscak.belablok.interfaces.Common
import com.zagorscak.belablok.interfaces.onNoteClickListener
import com.zagorscak.belablok.memento.GameMemento
import com.zagorscak.belablok.memento.GameState
import com.zagorscak.belablok.models.Note
import com.zagorscak.belablok.viewmodels.ScoreViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.thread

class ScoreActivity : AppCompatActivity(), onNoteClickListener {
    private lateinit var viewModel: ScoreViewModel
    private val noteAdapter: NoteAdapter = NoteAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
            .create(ScoreViewModel::class.java)
        setTheme()
        setContentView(R.layout.activity_main)
        loadDataFromSharedPreferences()
        setRecyclerView()
        viewModel.getAllNotesFromRoom().observe(this, Observer {
            if (!checkVictory(it)) {
                noteAdapter.addNotes(it)
                tv_team1Points.text = viewModel.getTeam1TotalPoints(it)
                tv_team2Points.text = viewModel.getTeam2TotalPoints(it)
            } else {
                onVictory()
            }
        })
        viewModel.toast.observe(this, Observer {
            showToast(it)
        })
        setListeners()
    }

    private fun setTheme() {
        if (viewModel.isEnabledDarkMode()) {
            setTheme(R.style.AppTheme_Dark)
        } else {
            setTheme(R.style.AppTheme_Light)
        }
    }

    private fun loadDataFromSharedPreferences() {
        val team1Score = viewModel.getTeam1Score()
        val team2Score = viewModel.getTeam2Score()
        tv_team1Score.text = team1Score.toString()
        tv_team2Score.text = team2Score.toString()
    }

    private fun setRecyclerView() {
        rc_Score.layoutManager = LinearLayoutManager(this)
        rc_Score.adapter = this.noteAdapter

        val itemTouchHelperCallBack = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                android.app.AlertDialog.Builder(this@ScoreActivity).setTitle("Upozorenje")
                    .setMessage("Sigurno obrisati odabrani zapis?")
                    .setPositiveButton("Da") { dialog, which ->
                        viewModel.setState(
                            GameState(
                                viewModel.getTeam1Score(),
                                viewModel.getTeam2Score(),
                                noteAdapter.getAllNotes()
                            ).saveState().deepCopy()
                        )
                        val position = viewHolder.adapterPosition
                        val noteToDelete = noteAdapter.getNote(position)
                        noteAdapter.deleteNote(position)
                        tv_team1Points.text =
                            viewModel.getTeam1TotalPoints(noteAdapter.getAllNotes())
                        tv_team2Points.text =
                            viewModel.getTeam2TotalPoints(noteAdapter.getAllNotes())
                        thread {
                            Thread.sleep(Common.rcAnimationDuration)
                            viewModel.deleteNote(noteToDelete)
                        }
                    }
                    .setNegativeButton("Ne") { dialog, which ->
                        noteAdapter.notifyItemChanged(viewHolder.adapterPosition)
                        dialog.dismiss()
                    }.setOnCancelListener {
                        noteAdapter.notifyItemChanged(viewHolder.adapterPosition)
                    }.create().show()
            }
        }
        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallBack)
        itemTouchHelper.attachToRecyclerView(rc_Score)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activitymain_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        super.onPrepareOptionsMenu(menu)
        when (viewModel.getGamePoints()) {
            501 -> menu?.getItem(1)?.subMenu?.getItem(0)?.isChecked = true
            701 -> menu?.getItem(1)?.subMenu?.getItem(1)?.isChecked = true
            1001 -> menu?.getItem(1)?.subMenu?.getItem(2)?.isChecked = true
        }
        menu?.getItem(2)?.isChecked = viewModel.isEnabledDarkMode()
        menu?.getItem(3)?.isEnabled = viewModel.isPreviousStateAvailable()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_reset -> {
                AlertDialog.Builder(this).setMessage("Sigurno želite resetirati zapisnik?")
                    .setTitle("Upozorenje")
                    .setPositiveButton("Da") { dialog, which ->
                        viewModel.setState(
                            GameState(
                                viewModel.getTeam1Score(),
                                viewModel.getTeam2Score(),
                                noteAdapter.getAllNotes()
                            ).saveState().deepCopy()
                        )
                        resetAllData()
                    }
                    .setNegativeButton("Ne") { dialog, which ->
                        dialog.dismiss()
                    }.create().show()
                return true
            }
            R.id.item_darkMode -> {
                item.isChecked = !item.isChecked
                viewModel.setDarkMode(item.isChecked)
                setTheme()
                recreate()
                return true
            }
            R.id.item_game501 -> {
                item.isChecked = true
                if (noteAdapter.getAllNotes().size > 0) {
                    showWarningAboutChangingGamePoints(501)
                } else viewModel.saveGamePoints(501)
                return true
            }
            R.id.item_game701 -> {
                item.isChecked = true
                if (noteAdapter.getAllNotes().size > 0) {
                    showWarningAboutChangingGamePoints(701)
                } else viewModel.saveGamePoints(701)
                return true
            }
            R.id.item_game1001 -> {
                item.isChecked = true
                if (noteAdapter.getAllNotes().size > 0) {
                    showWarningAboutChangingGamePoints(1001)
                } else viewModel.saveGamePoints(1001)
                return true
            }
            R.id.item_undo -> {
                val previousState = viewModel.popState()
                if (previousState != null) {
                    restoreState(previousState)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun restoreState(state: GameMemento) {
        viewModel.saveTeam1Score(state.teamOneScore)
        viewModel.saveTeam2Score(state.teamTwoScore)
        loadDataFromSharedPreferences()
        viewModel.addNotes(state.Notes)
    }

    override fun onResume() {
        super.onResume()
        btn_newEntry.isEnabled = true
    }

    private fun setListeners() {
        btn_newEntry.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(this, R.anim.jump))
            btn_newEntry.isEnabled = false
            thread {
                Thread.sleep(Common.animationDuration)
                startActivity(Intent(this, NewEntryActivity::class.java))
            }
        }

        tv_team1Points.setOnClickListener {
            showToast(
                "Do izlaska nedostaje: ${viewModel.getTeam1PointsToFinish(noteAdapter.getAllNotes())} \nRazlika: ${
                    viewModel.getTeamsPointsDifference(
                        noteAdapter.getAllNotes()
                    )
                }"
            )
        }

        tv_team2Points.setOnClickListener {
            showToast(
                "Do izlaska nedostaje: ${viewModel.getTeam2PointsToFinish(noteAdapter.getAllNotes())} \nRazlika: ${
                    viewModel.getTeamsPointsDifference(
                        noteAdapter.getAllNotes()
                    )
                }"
            )
        }
    }

    //    prevention  from double clicking on reclycerview element because it opens 2 activities at same time
    private var lastClickTime: Long = 0

    override fun onNoteClick(position: Int) {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return
        }
        lastClickTime = SystemClock.elapsedRealtime()
        thread {
            val intent = Intent(this, NewEntryActivity::class.java)
            intent.putExtra(Common.NewEntryObjectKEY, noteAdapter.getNote(position))
            startActivity(intent)
        }
    }

    private fun checkVictory(notes: List<Note>): Boolean {
        if (viewModel.checkTeam1Victory(notes)) {
            val team1Score = viewModel.getTeam1Score()
            val newScore = team1Score + 1
            tv_team1Score.text = newScore.toString()
            viewModel.saveTeam1Score(newScore)
            return true
        }
        if (viewModel.checkTeam2Victory(notes)) {
            val team2Score = viewModel.getTeam2Score()
            val newScore = team2Score + 1
            tv_team2Score.text = newScore.toString()
            viewModel.saveTeam2Score(newScore)
            return true
        }
        return false
    }

    private fun onVictory() {
        noteAdapter.deleteAllNotes()
        tv_team1Points.text = "0"
        tv_team2Points.text = "0"
        viewModel.deleteAllNotes()
    }

    private fun resetAllData() {
        viewModel.deleteAllNotes()
        viewModel.saveTeam1Score(0)
        viewModel.saveTeam2Score(0)
        loadDataFromSharedPreferences()
    }

    private fun showWarningAboutChangingGamePoints(points: Int) {
        AlertDialog.Builder(this@ScoreActivity).setTitle("Upozorenje")
            .setMessage("Promjena bodova do izlaska usred igre može uzrokovati pobjedu, te se trenutni podaci neće moći vratiti. Sigurno želite promijeniti?")
            .setPositiveButton("Da") { dialog, which ->
                viewModel.popState()
                viewModel.saveGamePoints(points)
                if (checkVictory(noteAdapter.getAllNotes())) {
                    onVictory()
                }
            }
            .setNegativeButton("Ne") { dialog, which ->
                dialog.dismiss()
            }.create().show()
    }

    private fun showToast(message: String) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        val view = toast.view.findViewById<View>(android.R.id.message) as TextView
        view.gravity = Gravity.CENTER
        toast.show()
    }
}
