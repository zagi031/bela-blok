package com.zagorscak.belablok.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.zagorscak.belablok.R
import com.zagorscak.belablok.interfaces.Common
import com.zagorscak.belablok.interfaces.onNoteClickListener
import com.zagorscak.belablok.models.Note
import kotlinx.android.synthetic.main.note_holder.view.*
import kotlin.concurrent.thread

class NoteAdapter(private val noteClickListener: onNoteClickListener) :
    RecyclerView.Adapter<NoteViewHolder>() {

    private val notes: MutableList<Note> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.note_holder, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return notes.size
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(notes[position], noteClickListener)
    }

    fun getAllNotes() = notes

    fun getNote(position: Int): Note {
        return notes[position]
    }

    fun addNotes(notes: List<Note>) {
        this.notes.clear()
        this.notes.addAll(notes)
        notifyDataSetChanged()
    }

    fun addNote(note: Note) {
        notes.add(note)
        notifyItemInserted(notes.size - 1)
    }

    fun updateNote(note: Note, position: Int) {
        notes[position] = note
        notifyItemChanged(position)
    }

    fun deleteNote(position: Int) {
        notes.removeAt(position)
        notifyItemRemoved(position)
    }

    fun deleteAllNotes() {
        notes.clear()
        notifyItemRangeRemoved(0, notes.size)
    }
}

class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(note: Note, listener: onNoteClickListener) {
        itemView.tv_team1NotePoints.text = note.getTeam1TotalPoints().toString()
        itemView.tv_team2NotePoints.text = note.getTeam2TotalPoints().toString()

        itemView.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(listener as Context, R.anim.scale))
            thread {
                Thread.sleep(Common.animationDuration)
                listener.onNoteClick(adapterPosition)
            }

        }
    }
}