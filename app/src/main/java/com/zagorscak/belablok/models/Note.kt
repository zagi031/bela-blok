package com.zagorscak.belablok.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


@Entity(tableName = "note_table")
class Note(
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    var team1GamePoints: Int,
    var team1CallPoints: Int,
    var team2GamePoints: Int,
    var team2CallPoints: Int
) : Serializable {

    constructor() : this(0, 0, 0, 0, 0)

    fun getTeam1TotalPoints() = team1GamePoints + team1CallPoints
    fun getTeam2TotalPoints() = team2GamePoints + team2CallPoints
}