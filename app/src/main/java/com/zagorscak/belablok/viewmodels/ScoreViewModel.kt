package com.zagorscak.belablok.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.zagorscak.belablok.managers.PreferenceManager
import com.zagorscak.belablok.memento.GameMemento
import com.zagorscak.belablok.memento.MementoCareTaker
import com.zagorscak.belablok.models.Note
import com.zagorscak.belablok.room.NoteRoomDatabase
import kotlin.math.abs

class ScoreViewModel(application: Application) : AndroidViewModel(application) {
    private val _toast: MutableLiveData<String> = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast
    private val noteDao = NoteRoomDatabase.getInstance().noteDao()
    private val sharedPreferencesManager: PreferenceManager = PreferenceManager()
    private var gamePoints: Int = sharedPreferencesManager.getGamePoints()
    private val mementoCareTaker = MementoCareTaker.getInstance()

    fun setState(state: GameMemento) {
        mementoCareTaker.setState(state)
    }

    fun popState(): GameMemento? {
        return mementoCareTaker.popState()
    }

    fun isPreviousStateAvailable() = mementoCareTaker.isStateAvailable()

    fun isEnabledDarkMode() = sharedPreferencesManager.getDarkMode()

    fun setDarkMode(value: Boolean) = sharedPreferencesManager.saveDarkMode(value)

    fun saveGamePoints(points: Int) {
        this.gamePoints = points
        sharedPreferencesManager.saveGamePoints(points)
    }

    fun getGamePoints() = this.gamePoints

    fun getTeam1Score() = sharedPreferencesManager.getTeam1Score()

    fun saveTeam1Score(score: Int) = sharedPreferencesManager.saveTeam1Score(score)

    fun getTeam2Score() = sharedPreferencesManager.getTeam2Score()

    fun saveTeam2Score(score: Int) = sharedPreferencesManager.saveTeam2Score(score)

    fun getAllNotesFromRoom() = noteDao.getAllNotes()

    fun checkTeam1Victory(notes: List<Note>): Boolean {
        val team1Points = getTeam1TotalPoints(notes).toInt()
        val team2Points = getTeam2TotalPoints(notes).toInt()
        if (team1Points >= gamePoints && team1Points > team2Points) {
            _toast.value = "Mi smo pobjedili"
            return true
        }
        return false
    }

    fun checkTeam2Victory(notes: List<Note>): Boolean {
        val team1Points = getTeam1TotalPoints(notes).toInt()
        val team2Points = getTeam2TotalPoints(notes).toInt()
        if (team2Points >= gamePoints && team2Points > team1Points) {
            _toast.value = "Vi ste pobjedili"
            return true
        }
        return false
    }

    fun getTeam1TotalPoints(notes: List<Note>): String {
        var result = 0
        notes.forEach {
            result += it.getTeam1TotalPoints()
        }
        return result.toString()
    }

    fun getTeam2TotalPoints(notes: List<Note>): String {
        var result = 0
        notes.forEach {
            result += it.getTeam2TotalPoints()
        }
        return result.toString()
    }

    fun getTeamsPointsDifference(notes: List<Note>) =
        abs(getTeam1TotalPoints(notes).toInt() - getTeam2TotalPoints(notes).toInt())

    fun getTeam1PointsToFinish(notes: List<Note>) = gamePoints - getTeam1TotalPoints(notes).toInt()

    fun getTeam2PointsToFinish(notes: List<Note>) = gamePoints - getTeam2TotalPoints(notes).toInt()

    fun addNotes(notes: List<Note>) {
        noteDao.deleteAllNotes()
        noteDao.insert(notes)
    }

    fun deleteNote(note: Note) = noteDao.delete(note)

    fun deleteAllNotes() = noteDao.deleteAllNotes()
}