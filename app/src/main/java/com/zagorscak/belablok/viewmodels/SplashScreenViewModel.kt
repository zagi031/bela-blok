package com.zagorscak.belablok.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.zagorscak.belablok.managers.PreferenceManager

class SplashScreenViewModel(application: Application) : AndroidViewModel(application) {
    private val sharedPreferencesManager: PreferenceManager = PreferenceManager()

    fun isEnabledDarkMode() = sharedPreferencesManager.getDarkMode()
}