package com.zagorscak.belablok.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zagorscak.belablok.managers.PreferenceManager
import com.zagorscak.belablok.memento.GameMemento
import com.zagorscak.belablok.memento.MementoCareTaker
import com.zagorscak.belablok.models.Note
import com.zagorscak.belablok.room.NoteRoomDatabase

class NewEntryViewModel(application: Application) : AndroidViewModel(application) {
    private val Note = Note()
    private val _note: MutableLiveData<Note> = MutableLiveData<Note>()
    val note: LiveData<Note>
        get() = _note
    private val _toast: MutableLiveData<String> = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast
    private val noteDao = NoteRoomDatabase.getInstance().noteDao()
    private val sharedPreferencesManager: PreferenceManager = PreferenceManager()

    fun getAllNotesFromRoom() = noteDao.getAllNotes()

    fun setNoteID(id: Long) {
        Note.id = id
    }

    fun setTeam1GamePoints(points: Int) {
        Note.team1GamePoints = points
        Note.team2GamePoints = 162 - points
        _note.value = this.Note
    }

    fun setTeam2GamePoints(points: Int) {
        Note.team2GamePoints = points
        Note.team1GamePoints = 162 - points
        _note.value = this.Note
    }

    fun setTeam1CallPoints(points: Int) {
        Note.team1CallPoints = points
        _note.value = this.Note
    }

    fun setTeam2CallPoints(points: Int) {
        Note.team2CallPoints = points
        _note.value = this.Note
    }

    fun addNote(): Boolean {
        if (Note.team1GamePoints > 0 || Note.team2GamePoints > 0) {
            noteDao.insert(Note)
            return true
        } else _toast.value = "Unesite bodove"; return false
    }

    fun editNote(): Boolean {
        if (Note.team1GamePoints > 0 || Note.team2GamePoints > 0) {
            noteDao.update(Note)
            return true
        } else _toast.value = "Unesite bodove"; return false
    }

    fun isEnabledDarkMode() = sharedPreferencesManager.getDarkMode()

    fun setState(state: GameMemento) {
        MementoCareTaker.getInstance().setState(state)
    }

    fun getTeam1Score() = sharedPreferencesManager.getTeam1Score()

    fun getTeam2Score() = sharedPreferencesManager.getTeam2Score()
}