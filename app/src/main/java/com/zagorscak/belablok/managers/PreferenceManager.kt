package com.zagorscak.belablok.managers

import android.app.Application
import android.content.Context
import com.zagorscak.belablok.BelaBlok
import com.zagorscak.belablok.interfaces.Common

class PreferenceManager {
    private val sharedPreferences = BelaBlok.ApplicationContext.getSharedPreferences(
        Common.SharedPreferencesKEY,
        Context.MODE_PRIVATE
    )

    fun saveGamePoints(value: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(Common.GamePointsKEY, value)
        editor.apply()
    }

    fun saveTeam1Score(value: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(Common.Team1ScoreKEY, value)
        editor.apply()
    }

    fun saveTeam2Score(value: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(Common.Team2ScoreKEY, value)
        editor.apply()
    }

    fun saveDarkMode(value: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean(Common.DarkModeKEY, value)
        editor.apply()
    }

    fun getGamePoints() = sharedPreferences.getInt(Common.GamePointsKEY, 1001)

    fun getTeam1Score() = sharedPreferences.getInt(Common.Team1ScoreKEY, 0)

    fun getTeam2Score() = sharedPreferences.getInt(Common.Team2ScoreKEY, 0)

    fun getDarkMode() = sharedPreferences.getBoolean(Common.DarkModeKEY, false)
}