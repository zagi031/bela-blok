package com.zagorscak.belablok

import android.app.Application
import android.content.Context

class BelaBlok : Application() {
    companion object {
        lateinit var ApplicationContext: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()
        ApplicationContext = this
    }
}