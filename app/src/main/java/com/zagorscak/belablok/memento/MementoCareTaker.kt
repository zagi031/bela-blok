package com.zagorscak.belablok.memento

import com.zagorscak.belablok.interfaces.Common

class MementoCareTaker private constructor() {

    companion object {
        private var instance: MementoCareTaker? = null
        fun getInstance(): MementoCareTaker {
            if (instance == null) {
                instance = MementoCareTaker()
            }
            return instance as MementoCareTaker
        }
    }

    private var state: GameMemento? = null

    fun setState(newState: GameMemento){
        this.state = newState
    }

    fun popState(): GameMemento? {
        val result = state
        state = null
        return result
    }

    fun isStateAvailable():Boolean {
        if(state == null){
            return false
        }
        return true
    }
}