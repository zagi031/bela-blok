package com.zagorscak.belablok.memento

import com.google.gson.Gson
import com.zagorscak.belablok.models.Note

//  no need for private setter because val can be initialized only once
class GameMemento(team1Score: Int, team2Score: Int, notes: List<Note>) {
    val teamOneScore: Int = team1Score
    val teamTwoScore: Int = team2Score
    val Notes: List<Note> = notes

    fun deepCopy():GameMemento{
        val JSON = Gson().toJson(this)
        return Gson().fromJson(JSON, GameMemento::class.java)
    }
}