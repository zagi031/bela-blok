package com.zagorscak.belablok.memento

import com.zagorscak.belablok.models.Note

class GameState(var team1Score: Int, var team2Score: Int, var notes: List<Note>) {

    fun saveState(): GameMemento {
        return GameMemento(
            team1Score,
            team2Score,
            notes
        )
    }

    fun restoreState(state: GameMemento?){
        if(state != null){
            this.team1Score = state.teamOneScore
            this.team2Score = state.teamTwoScore
            this.notes = state.Notes
        }
    }
}