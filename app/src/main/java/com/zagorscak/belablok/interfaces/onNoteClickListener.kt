package com.zagorscak.belablok.interfaces

interface onNoteClickListener {
    fun onNoteClick(position: Int)
}