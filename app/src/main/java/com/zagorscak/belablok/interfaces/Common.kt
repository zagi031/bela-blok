package com.zagorscak.belablok.interfaces


interface Common {
    companion object {
        const val NewEntryObjectKEY = "NewEntryObjectKEY"
        const val SharedPreferencesKEY = "SharedPreferencesKEY"
        const val Team1ScoreKEY = "Team1ScoreKEY"
        const val Team2ScoreKEY = "Team2ScoreKEY"
        const val GamePointsKEY = "GamePointsKEY"
        const val DarkModeKEY = "DarkModeKEY"
        const val animationDuration: Long = 200
        const val rcAnimationDuration: Long = 500
        const val MementoCareTakerBufferSize:Int = 3
        const val SplashScreenDuration:Long = 500
    }
}