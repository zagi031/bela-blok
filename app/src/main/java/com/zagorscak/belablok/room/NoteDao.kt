package com.zagorscak.belablok.room

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import com.zagorscak.belablok.models.Note

@Dao
interface NoteDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(note: Note)

    @Insert
    fun insert(notes: List<Note>)

    @Update
    fun update(note: Note)

    @Delete
    fun delete(note: Note)

    @Query("SELECT * FROM note_table")
    fun getAllNotes(): LiveData<MutableList<Note>>

    @Query("DELETE FROM note_table")
    fun deleteAllNotes()
}