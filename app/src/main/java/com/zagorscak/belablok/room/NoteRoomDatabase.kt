package com.zagorscak.belablok.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.zagorscak.belablok.BelaBlok
import com.zagorscak.belablok.models.Note

@Database(entities = [Note::class], version = 1, exportSchema = false)
abstract class NoteRoomDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteDao

    companion object {
        private const val NAME = "note_database"
        private var INSTANCE: NoteRoomDatabase? = null

        fun getInstance(): NoteRoomDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    BelaBlok.ApplicationContext,
                    NoteRoomDatabase::class.java,
                    NAME
                ).allowMainThreadQueries().build()
            }
            return INSTANCE as NoteRoomDatabase
        }
    }
}